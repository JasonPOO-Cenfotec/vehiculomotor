package cr.ac.ucenfotec.ui;

import cr.ac.ucenfotec.bl.Motor;
import cr.ac.ucenfotec.bl.Vehiculo;

import java.util.ArrayList;

public class UI {
     public static ArrayList<Vehiculo> listaDeAutos = new ArrayList<>();
    public static void main(String[] args) {

        Motor motor1 = new Motor("SE-0051",1600);
        Motor motor2 = new Motor("SE-0054", 2400);

        Vehiculo auto1 = new Vehiculo("XFC-001", "Toyota",motor1);
        Vehiculo auto2 = new Vehiculo("89898", "Nissan", motor2);
        Vehiculo auto3 = new Vehiculo("FRE-781", "Lexus", motor2);


        listaDeAutos.add(auto1);
        listaDeAutos.add(auto2);
        listaDeAutos.add(auto3);

        System.out.println("*** Imprimir toda la información de los autos *****");
        for (Vehiculo autoTemp:listaDeAutos) {
            System.out.println(autoTemp);
        }

        System.out.println("*** Imprimir solo la serie del motor de los autos *****");
        for (Vehiculo autoTemp:listaDeAutos) {
            System.out.println(autoTemp.obtenerSerieMotor());
        }
    }

    public static int existeAuto(String placa){

        for (Vehiculo autoTemp:listaDeAutos) {
            if(autoTemp.getPlaca().equals(placa)){
                return 1;
            }
        }
        return 0;
    }
}
