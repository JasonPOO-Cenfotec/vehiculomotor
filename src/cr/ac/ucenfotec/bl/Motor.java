package cr.ac.ucenfotec.bl;

public class Motor {

    private String serie;
    private int cilindros;

    public Motor() {
    }

    public Motor(String serie, int cilindros) {
        this.serie = serie;
        this.cilindros = cilindros;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public int getCilindros() {
        return cilindros;
    }

    public void setCilindros(int cilindros) {
        this.cilindros = cilindros;
    }

    public String toString() {
        return "Motor{" +
                "serie='" + serie + '\'' +
                ", cilindros=" + cilindros +
                '}';
    }
}
