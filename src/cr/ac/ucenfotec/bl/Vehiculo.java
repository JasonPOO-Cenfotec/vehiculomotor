package cr.ac.ucenfotec.bl;

import java.util.ArrayList;

public class Vehiculo {

    private String placa;
    private String marca;
    private Motor motor; // Relación de agregación simple

    public Vehiculo() {
    }

    public Vehiculo(String placa, String marca) {
        this.placa = placa;
        this.marca = marca;
    }

    public Vehiculo(String placa, String marca, Motor motor) {
        this.placa = placa;
        this.marca = marca;
        this.motor = motor;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Motor getMotor() {
        return motor;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }

    public String obtenerSerieMotor(){
        return this.motor.getSerie();
    }

    public String toString() {
        return "Vehiculo{" +
                "placa='" + placa + '\'' +
                ", marca='" + marca + '\'' +
                ", motor=" + motor +
                '}';
    }
}
